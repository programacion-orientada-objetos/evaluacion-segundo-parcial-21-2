﻿using System;
using System.Linq;
using CapaNegocios;

namespace CarritoDeCompras
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            GestionCliente cliente = new GestionCliente();
            cliente.GuardarCliente("1112223334", "Michael Jackson", "Zambrano Zambrano", "michael@email.ec", "123");
            cliente.GuardarCliente("2223334445", "Juana Maria", "Pueblo Zambrano", "juan@mail.com", "123");
            Console.WriteLine(cliente.ListarUsuarios());

            GestionProductos producto = new GestionProductos();
            Console.WriteLine(producto.ListarProductos());

            
        }


        /* GRUPO 1
            Utilizando expresiones lambda o linq cree los métodos en las capas respectivas que permitan mostrar por pantalla:
            1) Los clientes ordenados alfabeticamente de A a Z. (1 Puntos)
            2) Los clientes mayores de 18 años ordenados alfabeticamente de Z a A. (2 Puntos)
            3) El cliente cuyo apellido sea un parametro ingresado por el usuario a traves del teclado. (2 Puntos)
            4) El cliente de mayor edad. (2 Puntos)
            5) El promedio de edad de los clientes. (1 Puntos)
         */

        /* GRUPO 2
            Utilizando expresiones lambda o linq cree los métodos en las capas respectivas que permitan mostrar por pantalla:
            1) La existencia de un producto en particular dado su identificador. (1 Puntos)
            2) Ordenar los productos por precio, de menor a mayor. (1 Puntos)
            3) Un listado de productos que sea mayor a un precio proporcionado por el usuario y ordendarlos por precio de mayor a menor. (2 Puntos)
            4) El producto con precio mas bajo. (2 Puntos)
            5) El valor promedio de los precios de los productos. (2 Punto)
         */

        /* GRUPO 3
            Utilizando expresiones lambda o linq cree los métodos en las capas respectivas que permitan mostrar por pantalla:
            1) La cantidad total de todos los productos. (2 Puntos)
            2) La suma de todos los precios de los productos para conocer el valor del inventario. (1 Puntos)
            3) Listado de productos menores a un valor proporcionado y ordenarlos alfabeticamente de A a Z. (2 Puntos)
            4) Los productos ordenados por categoria y por precio de mayor a menor. (2 Puntos)
            5) Los productos ordenados alfabeticamente de A a Z. (1 Puntos)
         */
    }
}
